package com.example.apphola93;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

public class MonedaActivity extends AppCompatActivity {
    private EditText txtCantidad;
    private TextView txtResultado;
    private Spinner spnMoneda;
    private Button btnCalcular, btnLimpiar, btnCerrar;
    private int pos = 0; // seleccionar la posicion del Spinner


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_moneda);

        iniciarComponentes();
        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (txtCantidad.getText().toString().matches("")) {
                    Toast.makeText(getApplicationContext(),
                            "Falto capturar cantidad",
                            Toast.LENGTH_SHORT).show();
                } else {
                    float cantidad = Float.parseFloat(txtCantidad.getText().toString());
                    float total = 0.0f;

                    switch (pos) {
                        case 0: // pesos a dolar americano
                            total = cantidad * 0.060f;
                            break;
                        case 1: // pesos a dolar canadiense
                            total = cantidad * 0.082f;
                            break;
                        case 2: // pesos a euros
                            total = cantidad * 0.055f;
                            break;
                        case 3: // pesos a libras
                            total = cantidad * 0.047f;
                            break;
                        default:
                            Toast.makeText(getApplicationContext(),
                                    "Moneda no soportada",
                                    Toast.LENGTH_SHORT).show();
                            return;
                    }
                    txtResultado.setText("Resultado: " + total);
                }
            }
        });

        spnMoneda.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                pos = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                txtResultado.setText("Resultado: ");
                txtCantidad.setText("");
                spnMoneda.setSelection(0);
            }
        });

        btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });
    }

    public void iniciarComponentes() {
        txtCantidad = findViewById(R.id.txtCantidad);
        txtResultado = findViewById(R.id.txtResultado);
        spnMoneda = findViewById(R.id.spnMonedas);
        btnCalcular = findViewById(R.id.btnCalcular);
        btnLimpiar = findViewById(R.id.btnLimpiar);
        btnCerrar = findViewById(R.id.btnCerrar);

        // Generar el Adpatador, para llamarlo con el ArrayString
        ArrayAdapter<String> adapter = new ArrayAdapter<>(getApplicationContext(),
                android.R.layout.simple_spinner_item,
                getResources().getStringArray(R.array.moneda));
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnMoneda.setAdapter(adapter);
    }

}
